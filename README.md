# ng-linter

Explica como configurar linters para TS en proyectos Angular

Este proyecto ya tiene configurado TSlint, por haberlo creado con angular-cli. Basta hacer 

>$ npm install

para instalar las dependencias.

## 1.- Seteando reglas

El archivo base donde se configuran las reglas es **tslint.json** y se puede ubicar en la raíz del proyecto.

En el apartado rules, incluiremos las reglas a las que queramos hacerle override. 

Para poder ver el análisis que hace TSlint sobre nuestro proyecto, necesitaremos agregar las siguientes configuraciones:

- **En el package.json**: Sustituir el comando *lint: ng lint* por 

```
"lint": "tslint -c tslint.json --force 'src/**/*.ts'",
```

Aquí estamos ejecutando el linter con las reglas definidas, sobre todo los files .ts de nuestro directorio src y forzando a que termine su ejecución con --force así hayan errores detectados.

Luego al ejecutar en una cónsola, *npm run lint*, veremos lo siguiente:

![Informe de salida de tslint](tutorial_images/1_salida_tslint.png)


## 2.- Instalando plugins de VSCode:

Todos los errores visualizados anteriormente, pueden ser corregidos, valiendonos de una herramienta que provee nuestro IDE Visual Studio Code. Este plugin nos permite identificar en qué línea de nuestro código se efectuó la violación a la regla y nos da sugerencias de cómo corregirlo:

![Correcciones de Plugin TSlint](tutorial_images/2_fix_plugin.png)

Para ello, debemos instalar las siguientes herramientas en nuestro IDE Visual Studio Code:

### 2.1 **TSlint**
https://marketplace.visualstudio.com/items?itemName=ms-vscode.vscode-typescript-tslint-plugin

Una vez instalado, podrán visualizarse los fixes haciendo click sobre la línea que reclama y sucesivamente presionando *Ctrol + .*
Esto nos sugerirá cómo corregirlo de forma automática.


### 2.2 **SonarTS**

Este plugin nos informará de problemas más puntuales en nuestro código como bugs y patrones sospechosos que pudieran generar errores en el futuro. Estas reglas se dividen en:

- *Bug Detection*: Detecta extractos de código dónde es muy posible que se de un bug, ya sea por errores de transcripción, copy-paste, variables no utilizadas.
- *Code Smell Detection*: Este tipo de detección tiene que ver con extractos de código que tienen problemas de mantenibilidad.
- *Vulnerability*: Detecta vulnerabilidades en nuestro código. Ejemplo: Credenciales puestas en duro.

Para hacer uso de estas reglas, requerimos de la siguiente librería:  **tslint-sonarts**. La instalamos mediante el siguiente comando:


>$ npm install tslint-sonarts --save-dev


Adicionalmente, tenemos que incluir esta dependencia como extensión en nuestro file *tslint.json*, para que de esta forma, el linter empiece a tomar en consideración las reglas definidas por Sonar para Typescript:

```
...
// Agregar después de rules
"extends": ["tslint-sonarts"],

..
```

Y dentro de rules, excluir las siguientes reglas que no están siendo consideradas para esta primera versión:
```
    // ==================================================================================================
    // tslint-sonarts rules. Documentacion con ejemplos: 
    // https://github.com/SonarSource/SonarTS/tree/master/sonarts-core/docs/rules
    // These rules are part of the code smell  and bugs section of tslint-sonarts
    "cognitive-complexity": false,
    "max-switch-cases": false,
    "mccabe-complexity": false,
    "no-alphabetical-sort": false,
    "no-big-function": false,
    "no-collapsible-if": false,
    "no-collection-size-mischeck": false,
    "no-element-overwrite": false,
    "no-empty-array": false,
    "no-empty-destructuring": false,
    "no-ignored-initial-value": false,
    "no-ignored-return": false,
    "no-inconsistent-return": false,
    "parameters-max-number": false,
    "prefer-promise-shorthand": false,
    "prefer-type-guard": false,
    "use-type-alias": false
```

Al igual que en el apartado anterior, podrán visualizarse los fixes haciendo click sobre la línea que reclama y sucesivamente presionando *Ctrol + .* .

## BONUS: Prettier:
Esta herramienta en particular, no es considerado un linter, dado que su función se limita a auto formatear código en base a un set de reglas definidas. Para hacer uso de esta herramienta, se debe instalar lo siguiente en el proyecto:


>$ npm install --save-dev --save-exact prettier


Una vez instalado en el proyecto, creamos un file en la raíz del proyecto de nombre *.prettierrc*, en el que incluiremos las reglas con nuestras configuraciones:

```
{
  "bracketSpacing": true,
  "printWidth": 1,
  "semi": true,
  "singleQuote": true,
  "tabWidth": 2,
  "trailingComma": "all",
  "useTabs": false
}

```
De la misma forma, es recomendable agregar un file *.prettierignore* para evitar darle formato a archivos que no lo ameriten:

```
package.json

```
Seguidamente, instalaremos Prettier para VScode:

https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode


### Formas con las que formatear código:

**1.- Al guardar el file**: Cada vez que hagamos ctrol + S en el file, el código se autoformateará de acuerdo a las reglas que hayan sido configuradas. Para esto, basta con incluirlo en los settings de VScode para el proyecto en curso. Creamos el siguiente directorio(.vscode) y file (settings.json) y agregamos lo siguiente:

```
"[typescript]": {
    "editor.formatOnSave": true
}
```

**2.- Configurando un hook antes de hacer commit**: los hooks son scripts que se ejecutan de forma local antes o después de un commit o un push. Para asegurarnos que vamos a pushear un código limpio, podemos configurar un hook antes de hacer un commit que nos revise si efectivamente el código es apto para ser versionado. Para ello, nos valdremos de las siguientes herramientas:

```
>$ npm install npm-run-all husky pretty-quick --save-dev
```

- npm-run-all : Permite ejecutar más de un comando npm en paralelo o en secuencia.
- husky: Permite asociar un comando de npm con un hook de git, en este caso pre-commit.
- pretty-quick: Permite formatear con prettier todos los files que estén en stage.


Una vez se termine de instalar todas las herramientas, agregamos el siguiente atributo a nuestro *package.json*:
Primero agregamos esto en nuestros scripts de comandos:
```
"format:fix": "pretty-quick --staged",
"precommit": "run-s format:fix lint"

```
Y al final del file, agregamos lo siguiente:
```
"husky": {
    "hooks": {
      "pre-commit": "npm run precommit"
    }
  }
```

Ahora al hacer commit, se formateará automáticamente.












